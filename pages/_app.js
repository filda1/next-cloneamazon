import '../styles/globals.css'
import "../styles/header.css"

import { AuthProvider } from '../contexts/AuthContext'

import { StateProvider } from "../contexts/StateProvider";
import reducer, { initialState } from "../reducers/reducer";

//Bostrapt
import 'bootstrap/dist/css/bootstrap.css'
//import "../css/customcss.css";

function MyApp({ Component, pageProps }) {
  return (
    <StateProvider intialState={initialState} reducer={reducer}>
     <AuthProvider>
      <Component {...pageProps} />
     </AuthProvider>
    </StateProvider>
  )
}

export default MyApp
