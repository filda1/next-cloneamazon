
import Carousel_home from "../components/carousel/Carousel_home"
import Header from "../components/header/Header"
import Home from "../components/Home"
import Footer from "../components/Footer"

function index() {
    return (
        <div>
            <Header/>
           {/* <Carousel_home/> */}
            <Home/>
            <Footer/>
        </div>
    )
}

export default index
