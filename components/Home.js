import React from 'react'
import { useEffect } from 'react'

import Product from "./Product"



function Home() {

    useEffect(() => (
        slider(0)
    ), [])

    return (

        <div className="home">
        <div className="home__container">
            {/* Image Banner */}
     
            <div className="home__slider-container">
                <div className="home__slide">
                    <img
                        className="home__image "
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/skillsstore/2020/August/Medh_Alexa_GW_3000x1200._CB405585145_.jpg"
                        alt="image0" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image "
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/img17/AmazonDevices/2019/Post_AugArt/GW_Echo_PC_2x_V2._CB405879256_.jpg"
                        alt="image1" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image"
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/img20/Wireless/SamsungM/M51/8thSept_GW/P38983965_IN_WLME_SamsungGalaxy_M51_New_Launch_DesktopTallHero_2_1500x600._CB405103024_.jpg"
                        alt="image2" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image"
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/AmazonVideo/2020/X-site/Multititle/Aug/1500x600_Hero-Tall_np._CB404803728_.jpg"
                        alt="image3" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image"
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/AmazonVideo/2020/X-site/SingleTitle/TheBoyss2/3000x1200_Hero-Tall_p._CB404993994_.jpg"
                        alt="image4" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image"
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/img20/Wireless/SamsungM/M51/GWTO/Pre_Launch/P38983965_IN_WLME_SamsungGalaxy_M51_New_Launch_M51_tallhero_1500x600_1._CB405468917_.jpg"
                        alt="image5" />
                </div>
                <div className="home__slide">
                    <img
                        className="home__image"
                        src="https://images-eu.ssl-images-amazon.com/images/G/31/img19/AmazonPay/Rajeshwari/September/GWBanners/Control/DesktopHero_1500x600._CB405007888_.jpg"
                        alt="image6" />
                </div>
            </div>
            {/* Product id, title, price, rating, image */}
            <div className="home__row">
                {/* Product */}
                <Product
                    id={12320}
                    title="The Lean Startup"
                    price={10}
                    rating={4.5}
                    image="https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._AC_SY400_.jpg"
                />
                <Product
                    id={12321}
                    title="Apple Watch Series 3 (GPS, 42mm) - Space Grey Aluminium Case with Black Sport Band"
                    price={20}
                    rating={5}
                    image="https://images-eu.ssl-images-amazon.com/images/I/41RLXO5JUhL._AC_SX368_.jpg"
                />
                <Product
                    id={12334}
                    title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
                    price={98.99}
                    rating={3.5}
                    image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
                />
            </div>
            <div className="home__row">
                {/* Product */}
                <Product
                    id={12323}
                    title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor"
                    price={199.99}
                    rating={3}
                    image="https://images-na.ssl-images-amazon.com/images/I/71Swqqe7XAL._AC_SX466_.jpg"
                />
                <Product
                    id={12324}
                    title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
                    price={98.99}
                    rating={2}
                    image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
                />
                <Product
                    id={3254354345}
                    title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
                    price={598.99}
                    rating={4}
                    image="https://images-na.ssl-images-amazon.com/images/I/816ctt5WV5L._AC_SX385_.jpg"
                />
            </div>
            <div className="home__row">
                {/* Product */}
                <Product
                    id={90829332}
                    title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor - Super Ultra Wide Dual WQHD 5120 x 1440"
                    price={1094.98}
                    rating={4}
                    image="https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SX355_.jpg"
                />
            </div>
            <div className="home__row">
                {/* Product */}
                <Product
                    id={12326}
                    title="Apple Watch Series 3 (GPS, 42mm) - Space Grey Aluminium Case with Black Sport Band"
                    price={20}
                    rating={5}
                    image="https://images-eu.ssl-images-amazon.com/images/I/41RLXO5JUhL._AC_SX368_.jpg"
                />
                <Product
                    id={12328}
                    title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
                    price={598.99}
                    rating={4}
                    image="https://images-na.ssl-images-amazon.com/images/I/816ctt5WV5L._AC_SX385_.jpg"
                />
                <Product
                    id={12327}
                    title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
                    price={98.99}
                    rating={5}
                    image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
                />
                <Product
                    id={12329}
                    title="The Lean Startup"
                    price={10}
                    rating={4.5}
                    image="https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._AC_SY400_.jpg"
                />
            </div>
        </div>
    </div>
 )
       {/* <div className="home">
        <div className="home-container">
            

            <div className="home-row">
                <Product 
                    id = {122322}
                    title="A Game of Thrones (Song of Ice and Fire) Hardcover – August 1, 1996" 
                    price = {26.29}
                    image="https://kbimages1-a.akamaihd.net/a26bb671-977c-4324-a6af-486847cdbe32/1200/1200/False/a-game-of-thrones-a-song-of-ice-and-fire-book-1.jpg"
                    rating={5}
                    author="by George R.R. Martin"
                    subtitle="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi"

                />
                <Product 
                    id = {122323}

                    title="Acer Aspire 5 Slim Laptop, 15.6 inches Full HD IPS Display, AMD Ryzen 3 3200U, Vega 3 Graphics, 4GB DDR4, 128GB SSD, Backlit Keyboard, Windows 10 in S Mode, A515-43-R19L, Silver" 
                    price = {364.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/71vvXGmdKWL._AC_SL1500_.jpg"
                    rating={4}
                    author="by Acer"

                />
            </div>

            <div className="home-row">
            <Product 
                    id = {122325}
                    title="SAMSUNG 75-inch Class QLED Q70T Series - 4K UHD Dual LED Quantum HDR Smart TV with Alexa Built-in (QN75Q70TAFXZA, 2020 Model)" 
                    price = {100.29}
                    image="https://images-na.ssl-images-amazon.com/images/I/51d1lfcMIaL._AC_SL1000_.jpg"
                    rating={4}
                    author="by Samsung"
                    
            />
            <Product 
                    id = {122326}
                    title="OtterBox Defender Series Case for iPad Pro 11(1st Gen) - Retail Packaging - Black" 
                    price = {11.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/51o%2ByhvbK-L._AC_SL1000_.jpg"
                    rating={5}
                    author="by OtterBox Store"
                    subtitle="At vero eos et accusamus et iusto odio dignissimos ducimus"

                />
            <Product 
                    id = {122321}
                    title="Samsung Gear S3 Frontier Smartwatch (Bluetooth), SM-R760NDAAXAR" 
                    price = {9.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/61GcJ2DDvIL._AC_SL1000_.jpg"
                    rating={3}
                    author="by Samsung Electronics Store"
                    subtitle="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis"

                />
            </div>

            <div className="home-row">
                <Product 
                    id = {122327}
                    title="LG 65UN7300PUF Alexa Built-In UHD 73 Series 65 4K Smart UHD TV (2020" 
                    price = {300.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/81ZcNYPYthL._AC_SL1500_.jpg"
                    rating={5}
                    author="by LG Store"
                    subtitle="At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis"

                />
            </div>
        </div>
    </div>*/}

   
}

function slider(counter) {
    const slides = document.querySelectorAll(".home__image");

    slides.forEach((slide, index) => {
        if (index !== counter) {
            slide.style.visibility = `hidden`
            slide.classList.add(`image-${index}`)
        }
    })
    moveCorousal(counter, slides, slides.length)
}

function moveCorousal(counter, slides, len) {

    if (slides) {

        if (counter >= len - 1)
            counter = 0
        else
            counter += 1

        slides.forEach((slide, index) => {
            if (index === counter) {
                slide.style.visibility = `visible`
            }
            else {
                slide.style.visibility = `hidden`
            }
        })

    }
    setTimeout(() => {
        moveCorousal(counter, slides, len);
    }, 5000)

/*    parseInt(counter) % 5 === 0 ? (
        setTimeout(() => {
            toast.info(`${faker.name.findName()} added new product to cart`, {
                position: "bottom-left"
            });
        }, 10500)
    ) : (
            setTimeout(() => {
            }, 21000)
        )
   */
}

export default Home
