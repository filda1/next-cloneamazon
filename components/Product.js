import React from "react";

import StarIcon from "@material-ui/icons/Star";
import StarHalfIcon from '@material-ui/icons/StarHalf';
import StarOutlineIcon from '@material-ui/icons/StarBorder';

function Product({ id, title, price, rating, image }) {

  let halfRating = (rating - Math.floor(rating)) * 10;
  let outline = 0;

  halfRating > 0 ? outline = (5 - Math.ceil(rating)) : outline = (5 - Math.floor(rating))

   const addToBasket = () =>{
   /* dispatch({
        type: "ADD_TO_BASKET",
        item: {
          id: id,
          title: title,
          image: image,
          price: price,
          rating: rating,
        }
      })*/

   }

    return (

      <div className="product">
      <img src={image} alt={title} />
      <div className="product__info">
          <p>{title}</p>
          <div className="product__group">
              <p className="product__price">
                  <small>₹.</small>
                  <strong>{price}</strong>
              </p>
              <div className="product__rating">
                  {
                      Array(Math.floor(rating))
                          .fill()
                          .map((_, index) => (
                              <StarIcon key={index} />
                          ))
                  }
                  {
                      (halfRating > 0) ? <StarHalfIcon /> : <></>
                  }
                  {
                      outline > 0 ? (
                          Array(outline)
                              .fill()
                              .map((_, index) => (
                                  <StarOutlineIcon key={index} />
                              ))
                      )
                          : ""
                  }
              </div>
          </div>
      </div>

      <button onClick={addToBasket}>Add to Cart</button>
  </div>
 )
       {/* <div className="product">
        <div className="product-info">
          <p className="product-title">{title}</p>
          <p className="product-subtitle">{subtitle}</p>
          <p className="product-author">{author}</p>
          <div className="product-business">
            <div className="product-rating">
              {Array(rating)
                .fill()
                .map((_, i) => (
                  <StarIcon className="star-icon" />
                ))}
            </div>
  
            <p className="product_price">
              <h2>$ {price}</h2>
            </p>
          </div>
        </div>
  
        <img src={image}></img>
  
        <button onClick = {addToBasket}>Add to Basket</button>
      </div>
                */}
   
}

export default Product
