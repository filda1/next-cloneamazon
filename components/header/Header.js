import Link from 'next/link'
import { useContext, useState } from 'react'

import SearchIcon from '@material-ui/icons/Search';
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import MenuIcon from '@material-ui/icons/Menu';
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { InputGroup, FormControl, Form } from "react-bootstrap";
import { Drawer,Divider} from "antd";
import Badge from '@material-ui/core/Badge';

import { useStateValue } from "../../contexts/StateProvider"
import { AuthContext } from '../../contexts/AuthContext'
import { SpaRounded, SpaTwoTone } from '@material-ui/icons';
import Burger_menu from '../Burger_menu/Burger_menu';




function Header() {

    const [showMenu, setShowMenu] = useState(false);

    const [{ basket, current_user }, dispatch] = useStateValue();
    const { user } = useContext(AuthContext)

     const handleAuthentication = () => {
        /*if (user) {
            auth.signOut();
        }*/
     }

     const handleSearch = () => {

     }

    return (
    
        <header>
           <Burger_menu width={ 280 } />
          <div className="header">
             {/* Custom Logo */}
            <Link href="/">
              <img
                src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
                alt=""
                className="header_logo"
              />
            </Link>

              {/* Custom search bar */}
             <div className="header_search">
                <input type="text" className="header_search_input" />
                <SearchIcon className="header_search_icon"  fontSize="large"/>
             </div>

              {/* Right */}
             <div className="header_nav">
              <Link href= "/login">
               <div onClick={handleAuthentication} className="header_option">
                <span className="header_option_line_one">
                  Hello {!current_user ? "Guest" : current_user.email}
                </span>
                <span className="header_option_line_two">
                   {current_user ? "Sign Out" : "Sign in"}
                </span>
               </div>
              </Link>
            </div>

            <Link href="/orders">
              <div className="header_option">
               <span className="header_option_line_one">Returns</span>
               <span className="header_option_line_two">& Orders</span>
              </div>
            </Link>

            <div className="header_option">
             <span className="header_option_line_one">Your</span>
             <span className="header_option_line_two">Prime</span>
            </div>

            <Link href="/checkout">
             <div className="header_option_basket">

             <Badge badgeContent={ 2 } color="error" 
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
             >
             <ShoppingBasketIcon  /> 
            </Badge>
                  
              {/*<span className="header_option_basket_count">{basket?.length}</span>*/}
              <span>&nbsp;&nbsp;&nbsp;</span>
             </div>
            </Link>

          </div>
    
              {/* Bottom nav */}
              <div className="header_bottom_nav">
                {/*
                 <span className="header_link_menu_icon">
                    <MenuIcon className="h-6 mr-1" />      
                </span>
                 */}

                <span className="header_link_menu_icon">
                 
                </span>
                <span className="header_menu_all">All</span>
                <span className="header_link">Prime Video</span>
                <span className="header_link">Amazon Business</span>
                <span className="header_link">Today's Deals</span>
                <span className="header_link hidden lg:inline-flex">Electronics</span>
                <span className="header_link hidden lg:inline-flex">Foods & Grocery</span>
                <span className="header_link hidden lg:inline-flex">Prime</span>
                <span className="header_link hidden lg:inline-flex">Buy Again</span>
                <span className="header_link hidden lg:inline-flex">Shopper Toolkit</span>
                <span className="header_link hidden sm:inline-flex">
                    Health & Personal Care
                </span>
              </div>
          </header>
           
    )
}

export default Header
